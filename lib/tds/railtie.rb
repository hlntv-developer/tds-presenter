require 'rails/railtie'

module TDS
  class Railtie < Rails::Railtie

    initializer "tds.setup_action_controller" do |app|
      ActiveSupport.on_load :action_controller do
        TDS.setup_action_controller self
      end
    end

    console do
      require 'action_controller/test_case'
      ApplicationController.new.view_context
      TDS::ViewContext.build
    end
  end
end
