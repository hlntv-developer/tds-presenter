# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tds/version'

Gem::Specification.new do |spec|
  spec.name          = "tds"
  spec.version       = TDS::VERSION
  spec.authors       = ["HLN Developer"]
  spec.email         = ["hlntv@turner.com"]
  spec.summary       = %q{ Presenter that can reside in the controller file}
  spec.description   = %q{ Presenter that can reside in the controller file as opposed to the view file}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'activesupport', '>= 3.0'
  spec.add_dependency 'actionpack', '>= 3.0'
  spec.add_dependency 'request_store', '~> 1.0'

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency 'rspec', '~> 2.12'
  spec.add_development_dependency 'minitest-rails', '>= 1.0'
end
