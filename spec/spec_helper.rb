require 'bundler/setup'
require 'tds'
require 'rails/version'
require 'action_controller'
require 'action_controller/test_case'

RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.expect_with(:rspec) {|c| c.syntax = :expect}
  config.order = :random
end

def protect_module(mod)
  before { stub_const mod.name, mod.dup }
end
