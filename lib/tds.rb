require 'action_view'

require "tds/version"
require 'tds/delegation'
require 'tds/helper_proxy'
require 'tds/view_context'
require 'tds/railtie' if defined?(Rails)

module TDS
  def self.setup_action_controller(base)
    base.class_eval do
      include TDS::ViewContext

      before_filter :activate_view_context
    end
  end
end

